#!/bin/bash



if [ $# -eq 0 ]
  then
    echo "No arguments supplied" && exit 
fi

DEV1=$(cat ${1})
DEV2=$(cat ${2})

if [ $(($DEV1)) -ge $(($DEV2)) ]
then echo $(($DEV1))
else echo $(($DEV2))
fi

